$(function(){
  var hotels;
  var error;
  var review;

  $(document).ready(function() {
   $.getJSON("http://fake-hotel-api.herokuapp.com/api/hotels", function(json) {
     hotels = json;
   }).error(function() { alert("An error ocured"); });
  });

  $(document).ready(function() {
   $.getJSON("http://fake-hotel-api.herokuapp.com/api/reviews?hotel_id=13ef1108-7f18-40c7-ac0f-0e743b015755", function(json) {
     review = json;
  });
  });

function getDate(date){
    var month = new Date(date).getMonth();
    var day = new Date(date).getDay();
    var year = new Date(date).getFullYear();
    return (day+"."+month+"."+year);
  };

  var k = 0;

  $('#loadButton').on('click', function(){
    for(i=0; i<5 ; i++){
      if(k>=Object.keys(hotels).length){
        break;
      }
      var hotel = hotels[k];
      k++;
        switch (hotel.stars) {
          case 1: hotel.stars = '&#9733 &#9734 &#9734 &#9734 &#9734';
            break;
            case 2: hotel.stars = '&#9733 &#9733 &#9734 &#9734 &#9734';
              break;
              case 3: hotel.stars = '&#9733 &#9733 &#9733 &#9734 &#9734';
                break;
                case 4: hotel.stars = '&#9733 &#9733 &#9733 &#9733 &#9734';
                  break;
                  case 5: hotel.stars = '&#9733 &#9733 &#9733 &#9733 &#9733';
                    break;
          default:
        }
        var start = getDate(hotel.date_start);
        var end = getDate(hotel.date_end);
        for(var j = 0; j<Object.keys(review).length; j++){
          if(review[j].hotel_id === hotel.id){
            var picture;
            switch (review[j].positive) {
              case true: picture = 'glyphicon glyphicon-plus-sign';
                break;
                case false: picture = 'glyphicon glyphicon-minus-sign';
                  break;
              default:
            }
            var commentName = review[j].name;
            var comment = review[j].comment;
          } else {
            var commentName = "";
            var comment = "No hotel id specified";
          }
        }
        $('#hotelList').append('\
            <div class="col-md-12">\
              <div class="well well-sm contentHolder">\
                <div class="row">\
                  <div class="col-md-7">\
                    <img src="'+ hotel.images +'" class="img-responsive well-image imgHolder">\
                  </div>\
                  <div class="col-md-5 textHolder">\
                      <div class="row textHeading">\
                        <h2 class="hotelHeading" style=""><strong>' + hotel.name + '</strong> </h2>\
                        <p style="display: inline !important; float: right; margin-right: 20px;">' + hotel.stars + '</p>\
                        <p> ' + hotel.city + ' - ' + hotel.country + ' </p>\
                      </div>\
                      <div class="row textDescription">\
                        <p>' + hotel.description + ' </p>\
                      </div>\
                      <div class="row">\
                        <div class="col-md-6">\
                        <button class="showMore">Show reviews</button>\
                        </div>\
                        <div class="col-md-6">\
                          <p id="price">' + hotel.price + '€</p>\
                          <p style="float: right; margin-right: 20px;">' + start + ' - ' + end + '</p>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div class="row reviews">\
                    <div class="row">\
                     <div class="col-md-2">\
                        <span class="' + picture + '"></span>\
                     </div>\
                     <div class="col-md-10">\
                        <p><stron>' + commentName + '</stron></p>\
                        <p>' + comment + '</p>\
                     </div>\
                    </div>\
                  </div>\
              </div>\
          </div>\
        </div>\
            ');

    };
    $('.showMore').on('click', function(){
      $(this).parents().next('.reviews').slideToggle();
      if($(this).html() === 'Show reviews'){
        $(this).html('Hide reviews');
      } else {
        $(this).html('Show reviews');
      }
    });
  });

});
